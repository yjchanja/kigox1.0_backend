const connection = require('../databaseConnection.js');

async function getUserList(body){
    let conn, rows;
    try{
        conn = await connection.pool.getConnection();
        conn.query('USE test');
        rows = await conn.query('SELECT img_path as imgPath, title as title FROM tb_user WHERE USER_NO = ' + body.seq);
    }
    catch(err){
        throw err;
    }
    finally{
        if (conn) conn.end();
        return rows;
    }
}
 
module.exports = {
    getUserList: getUserList
}
