const mariadb = require('mariadb');
require('dotenv').config();

console.log('dbhost', process.env.DBhost);
console.log('DBport', process.env.DBport);
console.log('DBuser', process.env.DBuser);
console.log('DBpassword', process.env.DBpassword);
console.log('DBdatabase', process.env.DBdatabase);

const pool = mariadb.createPool({
    host: process.env.DBhost, port: process.env.DBport,
    user: process.env.DBuser, password: process.env.DBpassword,
    database: process.env.DBdatabase,
    connectionLimit: 5
});

module.exports = {
    pool : pool
}

