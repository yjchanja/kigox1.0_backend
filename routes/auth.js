var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');
require('dotenv').config();

const tokenKey = process.env.TokenKey;

router.post('/init', function (req, res) {
    try {
        console.log("id", req.body.id);
        console.log("pwd", req.body.pwd);

        let id = req.body.id;
        let pwd = req.body.pwd;

        let token = jwt.sign({
            id,
            pwd
        }, tokenKey, {
            expiresIn: '1m',
            issuer: 'token user'
        });

        return res.json({
            code: 200,
            message: 'token init',
            token
        })
    }
    catch (error) {
        return res.status(500).json({
            code: 500,
            message: 'token error'
        })
    }
});

module.exports = router;