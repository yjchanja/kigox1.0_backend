var express = require('express');
var router = express.Router();
var query = require('../public/database/query/mainQuery');
var { verifyToken } = require('./verifyToken');

router.post('/product', verifyToken, function (req, res, next) {
    console.log(req.body);
    try {
        query.getProductList(req.body)
        .then((rows) => {
            res.send(rows)
        })
        .catch((errMsg) => {
            console.log(errMsg);
        });
    }
    catch (error) {
        console.log(error);
    }
    
});

module.exports = router;