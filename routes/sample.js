var express = require('express');
var router = express.Router();
var movies = require('../sample.json');
var query = require('../public/database/query/sampleQuery');
var jwt = require('jsonwebtoken');

const tokenKey = 'jwtsecretkey';

const verifyToken = (req, res, next) => {
    console.log(req);
    console.log(req.headers);
    console.log(req.headers.authorization);
    try {
        req.decoded = jwt.verify(req.headers.authorization, tokenKey);
        return next();
    }
    catch (error) {
        if (error.name === 'TokenExpiredError') {
            return res.status(419).json({
              code: 419,
              message: 'token expired'
            });
        }
      
        // 토큰의 비밀키가 일치하지 않는 경우
        return res.status(401).json({
            code: 401,
            message: 'invalid token'
        });
    }
};

router.post('/', verifyToken, function (req, res, next) {
    console.log(req);
    console.log(req.body.seq);

    query.getUserList(req.body)
    .then((rows) => {
        res.send(rows)
    })
    .catch((errMsg) => {
        console.log(errMsg);
    });
});


router.post('/authInit', function (req, res) {
    try {
        // let id = 'test';
        // let pwd = 's12345';

        let token = jwt.sign({

        }, tokenKey, {
            expiresIn: '1m',
            issuer: 'token user'
        });

        return res.json({
            code: 200,
            message: 'token init',
            token
        })
    }
    catch (error) {
        return res.status(500).json({
            code: 500,
            message: 'token error'
        })
    }
});




// 영화 상세 페이지를 위한 코드
router.get('/:id', function (req, res, next) {
    console.log(2);
 var id = parseInt(req.params.id, 10)
 var movie = movies.filter(function (movie) {
 return movie.id === id
 });
 res.send(movie)
});
module.exports = router;