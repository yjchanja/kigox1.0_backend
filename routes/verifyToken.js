var jwt = require('jsonwebtoken');
require('dotenv').config();
const tokenKey = process.env.TokenKey;

const verifyToken = (req, res, next) => {
    try {
        req.decoded = jwt.verify(req.headers.authorization, tokenKey);
        return next();
    }
    catch (error) {
        if (error.name === 'TokenExpiredError') {
            return res.status(419).json({
              code: 419,
              message: 'token expired'
            });
        }
      
        // 토큰의 비밀키가 일치하지 않는 경우
        return res.status(401).json({
            code: 401,
            message: 'invalid token'
        });
    }
};

module.exports = {
    verifyToken : verifyToken
};